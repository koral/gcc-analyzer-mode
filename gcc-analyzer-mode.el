;;; gcc-analyzer-mode.el --- GCC static analyzer -*- lexical-binding: t; -*-

;; Copyright (C) 2020 Free Software Foundation, Inc.

;; Author: Andrea Corallo <akrl@sdf.org>
;; Keywords: languages, c

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'cl-lib)
(require 'json)
(require 'flymake)

(defgroup gccan nil
  "GCC analyzer output source navigator."
  :group 'languages)

(defcustom gccan-gcc-invoke-gcc nil
  "Try to invoke GCC ourself."
  :type 'string
  :group 'gccan)

(defcustom gccan-gcc-executable "gcc"
  "GCC executable."
  :type 'string
  :group 'gccan)

(defvar gccan-issues nil)
(make-variable-buffer-local 'gccan-issues)

(defvar gccan-curr-issue-num nil)
(make-variable-buffer-local 'gccan-curr-issue-num)

(defvar gccan-overlays nil)
(make-variable-buffer-local 'gccan-overlays)

(defvar gccan-curr-issue-step nil)
(make-variable-buffer-local 'gccan-curr-issue-step)

(cl-defstruct gccan-issue
  type text path locs)

(cl-defstruct gccan-step
  parent beg func text)

(defun gccan-version ()
  "Return the GCC version as (MAJOR MINOR PATCH)."
  (with-temp-buffer
    (call-process gccan-gcc-executable nil (current-buffer) nil "--version")
    (goto-char (point-min))
    (when (looking-at "^gcc (.*) \\([0-9]+\\).\\([0-9]+\\).\\([0-9]+\\)")
      (mapcar #'string-to-number
              (list (match-string 1) (match-string 2) (match-string 3))))))

(defun gccan-cleanup ()
  ""
  (mapc #'delete-overlay gccan-overlays)
  (setf gccan-curr-issue-step nil
	gccan-overlays ()))

(defun gccan-load-json ()
  ""
  (let ((file (read-file-name "Find GCC json file:" nil nil nil nil
			      (lambda (filename)
				(string-suffix-p ".json" filename)))))
    (with-temp-buffer
      (insert-file-contents file)
      (goto-char (point-min))
      (let ((json-array-type 'list)
	    (json-object-type 'hash-table))
	(json-read)))))

(defun gccan-invoke-gcc ()
  "Invoke the analyzer and fetch results."
  (unless (executable-find gccan-gcc-executable)
    (error "Cannot find a suitable GCC executable"))
  (let ((gcc-version (gccan-version))
	(source (buffer-file-name)))
    (when (< (car gcc-version) 10)
      (apply #'error "GCC %d.%d.%d was found but 10.0.0 is required"
	     gcc-version))
    (with-temp-buffer
      (call-process gccan-gcc-executable nil (current-buffer) nil "-S"
		    "-fanalyzer" "-fdiagnostics-format=json"
		    source "-o" (make-temp-file "gcc-analyzer-mode"))
      (goto-char (point-min))
      (let ((json-array-type 'list)
	    (json-object-type 'hash-table))
	(json-read)))))

(defun gccan-location-to-pos (loc)
  ""
  (+ (line-beginning-position (gethash "line" loc))
     (gethash "column" loc) -1))

(defun gccan-collect-path (issue path)
  ""
  (cl-loop with src = (buffer-file-name (current-buffer))
	   for step in path
	   for loc = (gethash "location" step)
	   when (string= (file-name-nondirectory src) (gethash "file" loc))
	   collect
	   (make-gccan-step :parent issue
			    :beg (gccan-location-to-pos loc)
			    :func (gethash "function" step)
			    :text (gethash "description" step))))

(defun gccan-collect-issue (issue)
  (save-excursion
    (goto-char (point-min))
    (cl-loop
     with an-issue = (make-gccan-issue
		      :type (intern (gethash "kind" issue))
		      :text (gethash "message" issue)
		      :locs ())
     for loc in (gethash "locations" issue)
     for caret = (gethash "caret" loc)
     for finish = (gethash "finish" loc)
     for beg = (gccan-location-to-pos caret)
     for end = (if finish
		   (1+ (gccan-location-to-pos finish)))
     for path = (gethash "path" issue)
     do
     (push (cons beg end) (gccan-issue-locs an-issue))
     (when path
       (setf (gccan-issue-path an-issue)
	     (gccan-collect-path an-issue path)))
     finally (cl-return an-issue))))

(defun gccan-goto-issue (n)
  "Goto issue number N."
  (when (or (> n (1- (length gccan-issues)))
	    (< n 0))
    (error "Issue number %d does not exists" n))
  (gccan-cleanup)
  (cl-loop with issue = (nth n gccan-issues)
	   for (beg . end) in (gccan-issue-locs issue)
	   for overlay = (make-overlay beg end)
	   do
	   (overlay-put overlay 'face (cl-ecase (gccan-issue-type issue)
					(error 'flymake-error)
					(warning 'flymake-warning)
					(note 'flymake-note)))
	   (push overlay gccan-overlays)
	   finally
	   (setf gccan-curr-issue-step issue
		 gccan-curr-issue-num n)
	   (message "Issue %d: %s" n (gccan-issue-text issue))))

(defun gccan-draw-step (step)
  (gccan-cleanup)
  (let* ((beg (gccan-step-beg step))
	 (end (progn
		(goto-char beg)
		(end-of-line)
		(point)))
	 (overlay (make-overlay beg end)))
    (overlay-put overlay 'face 'flymake-note)
    (setf gccan-overlays `(,overlay))))

(defun gccan-goto-step (n)
  "Goto the step number N into the current issue."
  (let ((step (nth n (gccan-issue-path gccan-curr-issue-step))))
    (gccan-draw-step step)
    (setf gccan-curr-issue-step step)
    (message "Step %d: %s" n (gccan-step-text step))))

(defun gccan-move-forward ()
  ""
  (interactive)
  (if (gccan-issue-p gccan-curr-issue-step)
      (gccan-goto-step 0)
    (cl-loop with issue = (gccan-step-parent gccan-curr-issue-step)
	     for i in (gccan-issue-path issue)
	     for n from 0 below (1- (length (gccan-issue-path issue)))
	     when (eq i gccan-curr-issue-step)
	     do
	     (setf gccan-curr-issue-step issue)
	     (gccan-goto-step (1+ n))
	     (cl-return)
	     finally (error "Last step reached"))))

(defun gccan-move-backward ()
  ""
  (interactive)
  (if (gccan-issue-p gccan-curr-issue-step)
      (error "Can't move backward")
    (let ((issue (gccan-step-parent gccan-curr-issue-step)))
      (cl-loop for i in (gccan-issue-path issue)
	       for n from 0
	       when (eq i gccan-curr-issue-step)
	       do
	       (setf gccan-curr-issue-step issue)
	       (if (= n 0)
		   (gccan-goto-issue gccan-curr-issue-num)
		 (gccan-goto-step (1- n)))
	       (cl-return)
	       finally (error "Incoherent issue or step")))))

(defun gccan-next-issue ()
  ""
  (interactive)
  (gccan-goto-issue (1+ gccan-curr-issue-num)))

(defun gccan-prev-issue ()
  ""
  (interactive)
  (gccan-goto-issue (1- gccan-curr-issue-num)))

;;;###autoload
(define-minor-mode gcc-analyzer-mode
  ""
  :group 'gccan
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map (kbd "f") #'gccan-move-forward)
	    (define-key map (kbd "b") #'gccan-move-backward)
	    (define-key map (kbd "n") #'gccan-next-issue)
	    (define-key map (kbd "p") #'gccan-prev-issue)
            map)
  (gccan-cleanup)
  (cond
   (gcc-analyzer-mode
    (setf gccan-issues
	  (mapcar #'gccan-collect-issue (if gccan-gcc-invoke-gcc
					    (gccan-invoke-gcc)
					  (gccan-load-json))))
    (gccan-goto-issue 0)
    (message "Found %d issues" (length gccan-issues))
    (setf buffer-read-only t))
   (t (setf buffer-read-only nil))))

(provide 'gcc-analyzer-mode)

;;; gcc-analyzer-mode.el ends here
